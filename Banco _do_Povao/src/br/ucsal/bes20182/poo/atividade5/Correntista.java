package br.ucsal.bes20182.poo.atividade5;


public class Correntista {

	private String nome;
	private Integer cpf;
	private Integer telefone;
	private String endereco;
	private double rendamensal;


	public Correntista() {
		super();
	}


	public Correntista(String nome, Integer cpf, Integer telefone, String endereco, double rendamensal) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendamensal = rendamensal;
	}


	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getCpf() {
		return cpf;
	}
	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}
	public Integer getTelefone() {
		return telefone;
	}
	public void setTelefone(Integer telefone) {
		this.telefone = telefone;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public double getRendamensal() {
		return rendamensal;
	}
	public void setRendamensal(double rendamensal) {
		this.rendamensal = rendamensal;
	}




}
