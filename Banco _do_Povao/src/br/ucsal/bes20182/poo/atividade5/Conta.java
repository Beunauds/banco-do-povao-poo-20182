package br.ucsal.bes20182.poo.atividade5;

public class Conta {

	private Integer numeroconta;
	private double saldo;
	private TipoContaEnum situacaodaconta;



	public Integer getNumeroconta() {
		return numeroconta;
	}

	public void setNumeroconta(Integer numeroconta) {
		this.numeroconta = numeroconta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}


	public TipoContaEnum getSituacaodaconta() {
		return situacaodaconta;
	}

	public void setSituacaodaconta(TipoContaEnum situacaodaconta) {
		this.situacaodaconta = situacaodaconta;
	}



}
